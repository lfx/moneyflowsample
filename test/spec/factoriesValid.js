'use strict';

describe('Factory: Valid', function() {

	var should = chai.should();
	var expect = chai.expect;
	var validator;
	var special = '!@#$%^&*()_+,.'.split(''); 

	beforeEach(module('MoneyFlow.factories'));

	beforeEach(inject(function($injector) {
		validator = $injector.get('Valid');
	}));

	it('should validate title and get true', function() {
		var titles = [
			'a', 'ab', 'ab12', '12', '123@', ' ab', ' a ', 'ą', 'ž'
		];
		titles = titles.concat(special);

		titles.forEach(function(title) {
			expect(validator.title(title)).to.be.true;
		});

	});

	it('should validate title and get false', function() {
		var titles = [
			'', ' ', '   ', '    ', ''
		];

		titles.forEach(function(title) {
			expect(validator.title(title)).to.be.false;
		});

	});

	it('should validate sum and get true', function() {
		var sums = [1, 1.1, .1, '1', '5', '   1', '   1.2', ' 1  ', '-1', '-1'];
		sums.forEach(function(sum) {
			expect(validator.sum(sum)).to.be.true;
		});
	})

	it('should validate sum and get false', function() {
		var sums = ['', ' ', '   ', '1b', 'bb', 'abc', '1e'];
		sums = sums.concat(special);

		sums.forEach(function(sum) {
			expect(validator.sum(sum)).to.be.false;
		});
	})

});