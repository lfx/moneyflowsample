'use strict';

describe('Controller: PetIndexCtrl', function() {

	var should = chai.should();
	var expect = chai.expect;

	// load the controller's module
	beforeEach(module('MoneyFlow.controllers'));

	var IncomeCtrl,
		scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope) {
		scope = $rootScope.$new();
		IncomeCtrl = $controller('IncomeCtrl', {
			$scope: scope
		});
	}));

	it('should attach a list of pets to the scope', function() {
		scope.incomes.should.have.length(2);
	});

	/*
	it('valid', function() {
		var $injector = angular.injector(['MoneyFlow.controllers']);
		var validator = $injector.get('Valid');
		// expect(validator.title("a")).to.be.false;
	});
*/
});