'use strict';

//http://stackoverflow.com/questions/13592259/mocking-my-own-service-in-a-unit-test
//http://stackoverflow.com/questions/14773269/injecting-a-mock-into-an-angularjs-service
// http://www.alexrothenberg.com/2013/08/06/how-to-unit-test-an-angular-app.html sinon
//http://quickleft.com/blog/angularjs-unit-testing-for-real-though
//http://cjohansen.no/talks/2011/xp-meetup/#37
//http://www.elijahmanor.com/unit-test-like-a-secret-agent-with-sinon-js/
//https://www.youtube.com/watch?v=qK-Z0oEdE4Y&feature=player_embedded
// http://chaijs.com/plugins/sinon-chai

describe("x", function() {

	var expect = chai.expect;
	var mockStorage;
	var mockIdGenerator;

	beforeEach(module('MoneyFlow.factories'));
	beforeEach(function() {
		module(function($provide) {
			$provide.value('storage', mockStorage);
			$provide.value('IdGenerator', mockIdGenerator);
		});
	});

	describe('empty set', function() {
		beforeEach(function() {
			mockStorage = {
				get: function() {
					return null;
				}
			};

		});

		it('should not return any values', inject(function(Safe) {
			expect(Safe.getAll()).to.eql({});
		}));

	});

	describe('with objects', function() {

		var test1 = {
			id: 1,
			title: 'a',
			sum: 1.1
		}

		beforeEach(function() {
			mockStorage = {
				get: function() {
					return {
						"1": test1
					};
				},
				set: sinon.spy()
			};

			mockIdGenerator = {
				getNextId: sinon.stub().returns("2")
			}
		});

		it('should return saved values', inject(function(Safe) {
			var val = Safe.getAll();
			var expected = {
				'1': test1
			}
			expect(val).to.eql(expected);
		}));

		it('should save objects', inject(function(Safe) {
			var expected = {
				'1': test1
			}

			Safe.saveAll();
			expect(mockStorage.set.called).to.be.true;
			expect(mockStorage.set).to.have.been.calledWith('Safe', expected);

		}));

		it('should add new object', inject(function(Safe) {

			var expected = {
				'1': test1
			}

			var newobj = {
				id: null,
				title: 'a',
				sum: 1.0
			};

			Safe.add(newobj);

			newobj.id = 2;
			expected['2'] = newobj;

			expect(mockStorage.set.called).to.be.true;
			expect(mockStorage.set).to.have.been.calledWith('Safe', expected);

			expect(Safe.getAll()).to.eql(expected);

		}));

		it('should update object', inject(function(Safe) {

			var expected = {
				'1': test1
			}
			test1.title = "xxxx";

			Safe.update(test1);

			expect(mockStorage.set.called).to.be.true;
			expect(mockStorage.set).to.have.been.calledWith('Safe', expected);

			expect(Safe.getAll()).to.eql(expected);

		}));

		it('should return single object', inject(function(Safe) {

			expect(Safe.getOne(1)).to.eql(test1);
			expect(Safe.getOne(0)).to.eql(undefined);

		}));


		it('should count sum 1', inject(function(Safe) {
			expect(Safe.getTotal()).to.eql(1.1);
		}));

		it('should count sum 2', inject(function(Safe) {

			var newobj = {
				id: null,
				title: 'a',
				sum: 99
			};

			Safe.add(newobj);

			expect(Safe.getTotal()).to.eql(100.1);
		}));

		it('should delte single entry', inject(function(Safe) {
			expect(_.size(Safe.getAll()) === 1).to.be.true;

			var newobj = {
				id: null,
				title: 'a',
				sum: 99
			};

			Safe.add(newobj);

			expect(_.size(Safe.getAll()) === 2).to.be.true;

			newobj.id = 2;
			Safe.remove(newobj);
			expect(_.size(Safe.getAll()) === 1).to.be.true;

		}));

		it('should remove all entries', inject(function(Safe) {

			expect(_.size(Safe.getAll()) === 1).to.be.true;

			Safe.removeAll();

			expect(_.size(Safe.getAll()) === 0).to.be.true;

		}));


		it('should remove all entries and still work', inject(function(Safe) {

			expect(_.size(Safe.getAll()) === 1).to.be.true;

			Safe.removeAll();

			expect(_.size(Safe.getAll()) === 0).to.be.true;

			var newobj = {
				id: null,
				title: 'a',
				sum: 99
			};

			Safe.add(newobj);

			expect(_.size(Safe.getAll()) === 1).to.be.true;

		}));


		describe('update callbacks on all methods', function() {

			it('should call callback on saveAll', inject(function(Safe) {
				var callback = sinon.spy();
				Safe.registerObserverCallbacks(callback);
				Safe.saveAll();
				expect(callback).to.have.been.called;
			}));

			it('should call callback on update', inject(function(Safe) {
				var callback = sinon.spy();

				Safe.registerObserverCallbacks(callback);

				var expected = {
					'1': test1
				}
				test1.title = "xxxx";

				Safe.update(test1);
				expect(callback).to.have.been.called;
			}));


			it('should call callback on add', inject(function(Safe) {
				var callback = sinon.spy();

				Safe.registerObserverCallbacks(callback);

				var newobj = {
					id: null,
					title: 'a',
					sum: 99
				};

				Safe.add(newobj);

				expect(callback).to.have.been.called;
			}));

			it('should call callback on delete', inject(function(Safe) {
				var callback = sinon.spy();

				Safe.registerObserverCallbacks(callback);

				var newobj = {
					id: 1,
					title: 'a',
					sum: 99
				};

				Safe.remove(newobj);

				expect(callback).to.have.been.called;
			}));

		});

	});

	describe('Safe', function() {

		it('should return empty new object', inject(function(Safe) {

			var realVal = Safe.getNew();
			var exp = {
				id: null,
				title: '',
				sum: null
			};

			expect(realVal).to.be.eql(exp)

		}));

	});

});