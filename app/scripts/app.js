'use strict';
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('MoneyFlow', ['ionic', 'config', 'MoneyFlow.controllers', 
	'MoneyFlow.factories', 'MoneyFlow.filters', 'angularLocalStorage', 'ngCordova'])


.provider('ngCordovax', function(){
	var mode = 'prod';
	return {
		setMode : function(value) {
			mode = value;
		},
		$get : function() {
			console.log('mode',mode);
			if(mode==='browser'){
				return ngCordovaMock;
			}else{
				return ngCordova;	
			}
			
		}
	}
})

.config(function(ngCordovaxProvider){
	ngCordovaxProvider.setMode('browser');
	console.log('modexx');
})

.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
})
	.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('app', {
				url: '/app',
				abstract: true,
				templateUrl: 'templates/menu.html',
				controller: 'AppCtrl'
			})
			.state('app.outcome', {
				url: '/outcome',
				views: {
					'menuContent': {
						templateUrl: 'templates/outcome.html',
						controller: 'OutcomeCtrl'
					}
				}
			})
			.state('app.income', {
				url: '/income',
				views: {
					'menuContent': {
						templateUrl: 'templates/income.html',
						controller: 'IncomeCtrl'
					}
				}
			})
			.state('app.options', {
				url: '/options',
				views: {
					'menuContent': {
						templateUrl: 'templates/options.html',
						controller: 'OptionsCtrl'
					}
				}
			});
		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/app/outcome');
	});