'use strict';
angular.module('MoneyFlow.factories', [])

.factory('Valid', function() {

	var filterFloat = function(value) {
		if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
			return Number(value);
		}
		return NaN;
	};

	return {
		title: function(title) {
			return title.trim().length > 0;
		},
		sum: function(sum) {
			sum = sum.toString().trim();
			var ret = !isNaN(parseFloat(sum)) && !isNaN(parseFloat(filterFloat(sum))) && parseFloat(sum) !== 0;
			return ret;
		}
	};

})

.factory('IdGenerator', function() {

	function nextId() {
		return moment().toDate().getTime();
	}

	return {
		getNextId: function() {
			return nextId();
		}
	};
})

.factory('Safe', function(IdGenerator, storage) {
	var service = {};
	var storeObj = {};
	var MAIN_KEY = 'Safe';
	var observerCallbacks = [];
	var saved = storage.get(MAIN_KEY);

	function notifyCallbacks() {
		angular.forEach(observerCallbacks, function(callback) {
			callback();
		})
	};

	window.st = storeObj;
	window.sr = service;

	if (saved) {
		storeObj = saved;
	}

	service.registerObserverCallbacks = function(callback) {
		observerCallbacks.push(callback);
	};

	service.getNew = function() {
		return {
			id: null,
			title: '',
			sum: null
		};
	};

	service.getAll = function() {
		return storeObj;
	};

	service.saveAll = function() {
		storage.set(MAIN_KEY, storeObj);
		notifyCallbacks();
	};

	service.getOne = function(id) {
		return storeObj[id];
	};

	service.add = function(item) {
		var id = IdGenerator.getNextId();
		item.id = id;
		storeObj[id] = item;
		service.saveAll();
	};

	service.update = function(item) {
		storeObj[item.id] = item;
		service.saveAll();
	};

	service.remove = function(item) {
		delete storeObj[item.id];
		service.saveAll();
	};

	service.removeAll = function() {
		storeObj = {};
		service.saveAll();
	}

	service.getTotal = function() {
		var retVal = 0;
		angular.forEach(storeObj, function(item) {
			retVal += parseFloat(item.sum);
		});
		return retVal;
	};

	return service;
})

.factory('FileSafe', function($cordovaFile) {
	var service = {};

	service.saveToFile = function() {
		var filePath = 'xxx.txt';
		$cordovaFile.createFile(filePath, true).then(function(result) {
			$scope.res = result;
		}, function(err) {
			$scope.res = err;

		});
	};
	
	return service;
})

;