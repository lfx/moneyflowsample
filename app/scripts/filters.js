'use strict';
angular.module('MoneyFlow.filters', [])

.filter('moment', function() {
    return function(dateString, format) {
        return moment(dateString).format(format);
    };
});