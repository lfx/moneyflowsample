'use strict';
angular.module('MoneyFlow.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, Safe, Valid) {
	$scope.come = {};

	$ionicModal.fromTemplateUrl('templates/enter.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	$scope.showAdd = function(item) {
		$scope.hasErrorTitle = false;
		$scope.hasErrorSum = false;
		if (item) {
			$scope.come = item;
		} else {
			$scope.come = Safe.getNew();
		}

		$scope.modal.show();
	};

	$scope.add = function() {
		var valid = true;
		if (!Valid.title($scope.come.title)) {
			valid = false;
			$scope.hasErrorTitle = 'has-error';
		}
		$scope.come.sum = $scope.come.sum.replace("-", "").replace("+", "");
		if (!Valid.sum($scope.come.sum)) {
			valid = false;
			$scope.hasErrorSum = 'has-error';
		}
		if (valid) {
			if ($scope.come.id === null) {
				Safe.add($scope.come);
			} else {
				Safe.update($scope.come);
			}

			$scope.closeModal();
		}
	};

})

.controller('OutcomeCtrl', function($scope, Safe, $ionicActionSheet) {

	$scope.outcomes = Safe.getAll();
	$scope.editOutcome;

	$scope.edit = function(outcome) {
		$scope.editOutcome = outcome;
		$scope.showModify();
	};

	Safe.registerObserverCallbacks(function() {
		$scope.total = Safe.getTotal();
	});

	$scope.total = Safe.getTotal();


	$scope.showModify = function() {

		var hideSheet = $ionicActionSheet.show({
			buttons: [{
				text: 'Edit'
			}],
			destructiveText: 'Delete',
			titleText: 'Modify entry?',
			cancelText: 'Cancel',
			cancel: function() {},
			buttonClicked: function() {
				$scope.showAdd(angular.copy($scope.editOutcome));
				hideSheet();
			},
			destructiveButtonClicked: function() {
				Safe.remove($scope.editOutcome);
				hideSheet();
			}
		});

	};

})

.controller('IncomeCtrl', function($scope) {
	$scope.incomes = [{
		title: 'XXXX',
		sum: '123'
	}, {
		title: 'YYYY',
		sum: '321'
	}];
})

.controller('OptionsCtrl', function($scope, Safe, $ionicLoading, $ionicPopup, FileSafe) {
	$scope.deleteAll = function() {

		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete all?',
			template: ''
		});
		confirmPopup.then(function(res) {
			if (res) {
				Safe.removeAll();
				$ionicLoading.show({
					template: 'All Deleted!',
					noBackdrop: true,
					duration: 2000
				});
			}
		});

	};

	$scope.saveToFile = function(){
		FileSafe.saveToFile();
	};
	
})

;